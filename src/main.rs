extern crate gio;
extern crate gtk;

use gio::prelude::*;
use gtk::prelude::*;

use std::env::args;

pub fn get_wayland_handle(win: gdk::Window) -> Option<u32> {
    use std::os::raw::{c_char, c_void};

    extern "C" {
        pub fn gdk_wayland_window_export_handle(
            window: *mut glib::object::GObject,
            cb: Option<
                unsafe extern "C" fn(*mut glib::object::GObject, *const c_char, *mut c_void),
            >,
            user_data: *mut c_void,
            destroy_notify: Option<unsafe extern "C" fn(*mut c_void)>,
        ) -> bool;
    }

    extern "C" fn callback(
        _window: *mut glib::object::GObject,
        handle: *const c_char,
        user_data: *mut c_void,
    ) {
        unsafe {
            *user_data = handle as *const u32;
        }
    }

    unsafe {
        let mut local = std::ptr::null_mut();
        if gdk_wayland_window_export_handle(
            win.as_ptr() as *mut _,
            Some(callback),
            local as *mut c_void,
            None,
        ) {
            Some(local as u32)
        } else {
            None
        }
    }
}

fn get_window_handle(win: gdk::Window) -> String {
    use gdk::WindowExt;

    match win.get_display().get_type().name().as_ref() {
        "GdkWaylandDisplay" => {
            let handle = get_wayland_handle(win).unwrap();
            format!("wayland:{}", handle)
        }
        "GdkX11Display" => {
            let xid = win.downcast::<gdkx11::X11Window>().map(|w| w.get_xid());
            format!("x11:{}", xid.unwrap())
        }
        _ => "".to_string(),
    }
}

fn main() {
    let application =
        gtk::Application::new(Some("com.github.gtk-rs.examples.basic"), Default::default())
            .expect("Initialization failed...");

    application.connect_activate(|app| {
        let window = gtk::ApplicationWindow::new(app);

        window.set_title("First GTK+ Program");
        window.set_border_width(10);
        window.set_position(gtk::WindowPosition::Center);
        window.set_default_size(350, 70);

        let button = gtk::Button::with_label("Click me!");
        button.connect_clicked(glib::clone!(@weak window => move |_| {
            println!("{:#?}", get_window_handle(window.get_window().unwrap()));
        }));
        window.add(&button);

        window.show_all();
    });

    application.run(&args().collect::<Vec<_>>());
}
